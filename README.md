# SSP Small Project

[Pages link](https://filda.kubik71.gitlab.io/swm_small_project/)

**GitLab Repository**, which uses the static site generator **MkDocs** in conjunction with continuous integration (CI).

CI generates HTML pages from Markdown sources. The CI also includes a quality test of Markdown files using *
*pymarkdownlnt**.

## Installation

```bash
pip install -r requirements.txt
```

## Dependencies

- [MkDocs](https://github.com/mkdocs/mkdocs)
- [mkdocs-material](https://github.com/squidfunk/mkdocs-material)
- [PyMarkdown](https://github.com/jackdewinter/pymarkdown)

---

## Hodnotící kritéria (CZ)

- [X] GIT repozitář na Gitlabu.
  - [X] Repozitář má README.md (K čemu projekt slouží, jak se používá, jak se instaluje, jaké jsou prerekvizity,
    apod.)
- [X] Použití libovolného generátoru statických stránek dle vlastního výběru. (MKdocs, Middleman, Jekyll, apod.)
- [X] Vytvořená CI v repozitáři.
- [X] CI má minimálně dvě úlohy:
  - [X] Test kvality Markdown stránek.
  - [X] Generování HTML stránek z Markdown zdrojů.
- [X] CI má automatickou úlohou nasazení web stránek (deploy).
- [X] Gitlab projekt má fukční web stránky s generovaným obsahem na URL:
